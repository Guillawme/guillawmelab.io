{
  "version": "https://jsonfeed.org/version/1.1",
  "title": "Guillaume",
  "description": "Recent content in Python on Guillaume",
  "home_page_url": "https://www.gaullier.org/en/tags/python/",
  "feed_url": "https://www.gaullier.org/en/tags/python/index.json",
  "authors": [
    {
      "name": "Guillaume Gaullier",
      "url": "https://www.gaullier.org/"
    }
  ],
  "user_comment": "This is a machine-readable full archive of my personal website.",
  "icon": "https://www.gaullier.org/images/logo.png",
  "favicon": "https://www.gaullier.org/favicon.png",
  "language": "en",
  "items": [
    
    {
      "id": "https://www.gaullier.org/en/blog/2021/05/09/new-tool-classconvergence/",
      "url": "https://www.gaullier.org/en/blog/2021/05/09/new-tool-classconvergence/",
      "title": "New tool: classconvergence",
      "authors": [
        {
          "name": "Guillaume Gaullier"
        }
      ],
      "date_published": "2021-05-09T00:00:00Z",
      "date_modified": "2021-05-09T00:00:00Z",
      "tags": ["work","Python","learning","cryo-em","visualization"],
      "content_html": "\u003cp\u003eLast week, I finally took time to finish and upload another tool for helping\nanalysis of cryo-EM data. This new tool is called \u003ccode\u003eclassconvergence\u003c/code\u003e and does\nthe same as \u003ca href=\"/en/blog/2020/11/06/new-tool-countparticles/\"\u003e\u003ccode\u003ecountparticles\u003c/code\u003e\u003c/a\u003e, but for every iteration of a 2D\nor 3D classification job from RELION instead of a single iteration. It then\nplots the number of particles per class as a function of iteration number. This\nis convenient to check whether a classification has converged (after which the\nnumber of particles per class stops changing).\u003c/p\u003e\n\u003cp\u003eThe tool can be installed \u003ca href=\"https://pypi.org/project/classconvergence\"\u003ewith pip\u003c/a\u003e, cited with\n\u003ca href=\"https://doi.org/10.5281/zenodo.4732050\"\u003edoi:10.5281/zenodo.4732050\u003c/a\u003e\n and the code is available \u003ca href=\"https://github.com/Guillawme/classconvergence\"\u003ehere\u003c/a\u003e.\u003c/p\u003e\n"
    }, 
    {
      "id": "https://www.gaullier.org/en/blog/2021/01/20/new-tool-topaztrainmetrics/",
      "url": "https://www.gaullier.org/en/blog/2021/01/20/new-tool-topaztrainmetrics/",
      "title": "New tool: topaztrainmetrics",
      "authors": [
        {
          "name": "Guillaume Gaullier"
        }
      ],
      "date_published": "2021-01-20T00:00:00Z",
      "date_modified": "2021-01-20T00:00:00Z",
      "tags": ["work","Python","learning","cryo-em","visualization"],
      "content_html": "\u003cp\u003eI made another command-line tool, this time to plot metrics from a training run\nof the \u003ca href=\"https://github.com/tbepler/topaz\"\u003eTopaz\u003c/a\u003e particle picking neural network.\u003c/p\u003e\n\u003cp\u003eThe tool can be  \u003ca href=\"https://pypi.org/project/topaztrainmetrics\"\u003einstalled with \u003ccode\u003epip\u003c/code\u003e\u003c/a\u003e. The code is available\n\u003ca href=\"https://github.com/Guillawme/topaztrainmetrics\"\u003ehere\u003c/a\u003e. I also made it citeable with \u003ca href=\"https://doi.org/10.5281/zenodo.4451826\"\u003edoi:10.5281/zenodo.4451826\u003c/a\u003e\n.\u003c/p\u003e\n"
    }, 
    {
      "id": "https://www.gaullier.org/en/blog/2020/11/06/new-tool-countparticles/",
      "url": "https://www.gaullier.org/en/blog/2020/11/06/new-tool-countparticles/",
      "title": "New tool: countparticles",
      "authors": [
        {
          "name": "Guillaume Gaullier"
        }
      ],
      "date_published": "2020-11-06T00:00:00Z",
      "date_modified": "2020-11-06T00:00:00Z",
      "tags": ["work","Python","learning","cryo-em"],
      "content_html": "\u003cp\u003eIn my \u003ca href=\"/en/blog/2020/10/18/new-tool-angdist/\"\u003eprevious post\u003c/a\u003e, I wrote that I would not bother writing a\nPython command-line program to simply count particles in each class in a\n\u003ccode\u003erun_data.star\u003c/code\u003e file from RELION, because it is straightforward to do \u003ca href=\"https://github.com/Guillawme/cryoEM-scripts/blob/master/count_particles.awk\"\u003ewith\nAWK\u003c/a\u003e (and it probably runs faster on large files). I\nchanged my mind and made \u003ca href=\"https://github.com/Guillawme/countparticles\"\u003ea new tool\u003c/a\u003e (also \u003ca href=\"https://pypi.org/project/countparticles\"\u003einstallable with\n\u003ccode\u003epip\u003c/code\u003e\u003c/a\u003e and citeable with\n\u003ca href=\"https://doi.org/10.5281/zenodo.4139778\"\u003edoi:10.5281/zenodo.4139778\u003c/a\u003e\n).\u003c/p\u003e\n\u003cp\u003eI still love the AWK solution for many reasons: it is indeed straightforward, it\nruns fast even on large files, there was no boilerplate code to write to handle\nfile input. And most importantly, it consists of only one file: drop it anywhere\nin your \u003ccode\u003e$PATH\u003c/code\u003e, make it executable, and it will work on any system that has AWK,\nwhich means everywhere, since \u003ca href=\"https://en.wikipedia.org/wiki/List_of_Unix_commands\"\u003eAWK is \u0026ldquo;mandatory\u0026rdquo; in the sense of IEEE Std\n1003.1-2008\u003c/a\u003e. The only problem is that star files from RELION change\nbetween versions, in a way that makes the relevant data for this counting not\nalways stored in the same column. And AWK can only refer to a column by index,\nnot by name. Changing the \u003ca href=\"https://github.com/Guillawme/cryoEM-scripts/blob/master/count_particles.awk#L20\"\u003ecolumn number in the AWK script\u003c/a\u003e is trivial, but\nwhen the script produces nonsensical output or no output at all while I am\ntrying to make sense of data, this kind of limitation gets frustrating quickly.\u003c/p\u003e\n\u003cp\u003eThe obvious solution was to write a Python program, because the \u003ca href=\"https://pypi.org/project/starfile\"\u003e\u003ccode\u003estarfile\u003c/code\u003e\nlibrary\u003c/a\u003e produces \u003ca href=\"https://en.wikipedia.org/wiki/Pandas_(software)\"\u003epandas\u003c/a\u003e \u003ccode\u003eDataFrame\u003c/code\u003es, and these in turn can\nrefer to a column by its name as defined in the star file. This works regardless\nof the column\u0026rsquo;s numerical index, so it doesn\u0026rsquo;t break when a new version of\nRELION produces star files in which the relevant column has a new index. The\ndownside is having to manage \u003ca href=\"https://xkcd.com/1987/\"\u003ea Python installation\u003c/a\u003e\u0026hellip; Luckily\n\u003ccode\u003econda\u003c/code\u003e makes this manageable, but it now seems like a way over-engineered\nsolution to the simple problem of counting lines by groups in a file\u0026hellip; so I\nalso added an option to display a bar graph representing the counts.\u003c/p\u003e\n"
    }, 
    {
      "id": "https://www.gaullier.org/en/blog/2020/10/18/new-tool-angdist/",
      "url": "https://www.gaullier.org/en/blog/2020/10/18/new-tool-angdist/",
      "title": "New tool: angdist",
      "authors": [
        {
          "name": "Guillaume Gaullier"
        }
      ],
      "date_published": "2020-10-18T00:00:00Z",
      "date_modified": "2020-10-18T00:00:00Z",
      "tags": ["work","learning","Python","visualization","cryo-em"],
      "content_html": "\u003cp\u003eI just finished putting together a command-line tool to plot a 2D histogram of\nEuler angles from a set of refined single particles from a cryo-EM dataset\n(\u003ccode\u003erun_*_data.star\u003c/code\u003e files from RELION). This is a useful visualization to\ncomplement the 3D histogram generated by RELION and displayed in relation to the\n3D reconstruction. I got the idea from cryoSPARC, which generates and displays\nthe exact same 2D histogram in its 3D refinement jobs (although with a color\nscale much less friendly to color blind people; I chose to use the \u0026ldquo;viridis\u0026rdquo;\ncolor scale by default).\u003c/p\u003e\n\u003cp\u003eThe tool can be  \u003ca href=\"https://pypi.org/project/angdist\"\u003einstalled with \u003ccode\u003epip\u003c/code\u003e\u003c/a\u003e. The code is available\n\u003ca href=\"https://github.com/Guillawme/angdist\"\u003ehere\u003c/a\u003e. I also made it citeable with \u003ca href=\"https://doi.org/10.5281/zenodo.4104053\"\u003edoi:10.5281/zenodo.4104053\u003c/a\u003e\n.\u003c/p\u003e\n\u003cp\u003eThe \u003ca href=\"https://pypi.org/project/starfile\"\u003e\u003ccode\u003estarfile\u003c/code\u003e library\u003c/a\u003e was very helpful. Having this easy way to\nread \u003ca href=\"https://en.wikipedia.org/wiki/Self-defining_Text_Archive_and_Retrieval\"\u003eSTAR\u003c/a\u003e files into Python gives me ideas for more tools to\nproduce other useful plots from data found in these files (basically, anything\nmore sophisticated than counting particles in each class, which is so easy to\ndo \u003ca href=\"https://github.com/Guillawme/cryoEM-scripts/blob/master/count_particles.awk\"\u003ewith AWK\u003c/a\u003e that I won\u0026rsquo;t bother making a Python command-line\ntool for this).\u003c/p\u003e\n"
    }, 
    {
      "id": "https://www.gaullier.org/en/blog/2019/12/13/new-tool-localres/",
      "url": "https://www.gaullier.org/en/blog/2019/12/13/new-tool-localres/",
      "title": "New tool: localres",
      "authors": [
        {
          "name": "Guillaume Gaullier"
        }
      ],
      "date_published": "2019-12-13T00:00:00Z",
      "date_modified": "2019-12-13T00:00:00Z",
      "tags": ["work","learning","Python","visualization","cryo-em"],
      "content_html": "\u003cp\u003eI just finished putting together a command-line tool to plot a histogram of\nlocal resolution values from a local resolution 3D map and a 3D mask\n(\u003ccode\u003erelion_locres.mrc\u003c/code\u003e and \u003ccode\u003emask.mrc\u003c/code\u003e files from RELION, respectively). This is a\nuseful visualization to complement a color-coded local resolution scale mapped\nonto the 3D reconstruction (or visualized across slices of said reconstruction)\nas usually shown in publications describing a cryo-EM structure. An example is\ndisplayed in Figure S3C of my \u003ca href=\"/en/blog/2019/11/19/new-preprint/\"\u003erecent preprint\u003c/a\u003e.\u003c/p\u003e\n\u003cp\u003eI got the idea from \u003ca href=\"https://twitter.com/biochem_fan/status/1161347681110962177\"\u003ea Twitter conversation\u003c/a\u003e, reused the\nsuggested code and wrapped it in a command-line tool that is documented and\n\u003ca href=\"https://pypi.org/project/localres\"\u003eeasy to install\u003c/a\u003e. The (very simple) code is available\n\u003ca href=\"https://github.com/Guillawme/localres\"\u003ehere\u003c/a\u003e. I also made it citeable with \u003ca href=\"https://doi.org/10.5281/zenodo.3575230\"\u003edoi:10.5281/zenodo.3575230\u003c/a\u003e\n.\u003c/p\u003e\n\u003cp\u003eThis was a good toy project to learn some basics of Python, especially to learn\nhow to use the \u003ca href=\"https://palletsprojects.com/p/click\"\u003eclick library\u003c/a\u003e to parse command-line arguments, how to\npackage a Python program and how to distribute it. I was surprised that anyone\ncan upload anything to the \u003ca href=\"https://pypi.org\"\u003ePython Package Index\u003c/a\u003e without any form of\nreview (in the R world, a review team checks every package submitted to the\n\u003ca href=\"https://cran.r-project.org/web/packages/policies.html\"\u003eCRAN repository\u003c/a\u003e).\u003c/p\u003e\n"
    }
  ]
}
